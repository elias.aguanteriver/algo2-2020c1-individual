#include "Lista.h"



Lista::Lista() : _long(0), _prim(NULL), _ult(NULL) {
}


Lista::Nodo::Nodo(const int& elem) : valor(elem) {
}


Lista::Lista(const Lista& l) : Lista() {
    *this = l;
}


Lista::~Lista() {
    _destruir();
}


void Lista::_destruir(){
    _ult = NULL;
    _long = 0;
    while(_prim != NULL) {
        Nodo* n = _prim;
        _prim= _prim->sig;
        delete n;
    }
}


Lista& Lista::operator=(const Lista& aCopiar) {
    Nodo* n = aCopiar._prim;
    _ult = NULL;
    _long = 0;
    while(_prim != NULL) {
        Nodo* m = _prim;
        _prim = _prim->sig;
        delete m;
    }
    for (int i = 0; i < aCopiar._long; i++){
        agregarAtras(n->valor);
        n = n->sig;
    }
    return *this;
}


void Lista::agregarAdelante(const int& elem) {
    Nodo* n = new Nodo(elem);
    if (_prim == NULL) {
        _prim = n;
        _ult = n;
        n->ant = NULL;
        n->sig = NULL;
    } else {
        n->ant = NULL;
        n->sig = _prim;
        _prim->ant = n;
        _prim = n;
    }
    _long++;
}


void Lista::agregarAtras(const int& elem) {
    Nodo* n = new Nodo(elem);
    if (_ult == NULL) {
        _prim = n;
        _ult = n;
        n->valor = elem;
        n->ant = NULL;
        n->sig= NULL;
    } else {
        n->valor = elem;
        n->ant = _ult;
        n->sig= NULL;
        _ult->sig = n;
        _ult = n;
    }
    _long++;
}


void Lista::eliminar(Nat i) {
    Nodo* n = _prim;
    if (_long == 1) {
        delete n;
        _prim = NULL;
        _ult = NULL;
    } else if (i == 0) {
        _prim = _prim->sig;
        delete n;
        _prim->ant= NULL;
    } else if (i == _long - 1) {
        n = _ult;
        _ult = _ult->ant;
        delete n;
        _ult->sig = NULL;

    } else {
        for (int j = 0; j != i; j ++){
            n = n->sig;
        }
        n->ant->sig = n->sig;
        n->sig->ant = n->ant;
        delete n;
    }
    _long--;
}


int Lista::longitud() const {
    int l = 0;
    Nodo* n = _prim;
    while (n != NULL) {
        n = n->sig;
        l++;
    }
    return l;
}


const int& Lista::iesimo(Nat i) const {
    Nodo* n = _prim;
    for (int j = 0; j != i; j++){
        n = n->sig;
    }
    return n->valor;
}


int& Lista::iesimo(Nat i) {
    Nodo* n = _prim;
    for (int j = 0; j != i; j++){
        n = n->sig;
    }
    return n->valor;
}


void Lista::mostrar(ostream& o) {
}