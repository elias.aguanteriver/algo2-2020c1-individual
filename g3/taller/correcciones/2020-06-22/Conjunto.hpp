
template <class T>
Conjunto<T>::Conjunto() : _cardinal(0), _raiz(NULL){
}

template <class T>
Conjunto<T>::Nodo::Nodo(const T& v) : valor(v), izq(NULL), der(NULL){
}

template <class T>
Conjunto<T>::~Conjunto() {
    if (_raiz != NULL) {
        destroyer(_raiz);
    }
}

template <class T>
void Conjunto<T>::destroyer(Nodo* n) {
    if (n->der != NULL) {
        destroyer(n->der);
    }
    if (n->izq != NULL) {
        destroyer(n->izq);
    }
    delete n;
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    Nodo* actual = _raiz;
    bool res = false;
    while (!res && actual != NULL) {
        if (clave < actual->valor){
            actual = actual->izq;
        } else if (clave > actual->valor) {
            actual = actual->der;
        } else if (actual->valor == clave) {
            res = true;
        }
    }
    return res;

}

template <class T>
void Conjunto<T>::insertar(const T& clave) {

    if (_cardinal == 0) {
        Nodo* nuevo = new Nodo(clave);
        _raiz = nuevo;
        _cardinal++;
    } else if (!pertenece(clave)) {
        Nodo* nuevo = new Nodo(clave);
        Nodo* actual = _raiz;
        Nodo* padreActual = _raiz;
        while (actual != NULL && clave != actual->valor) {
            if (clave < actual->valor) {
                padreActual = actual;
                actual = actual->izq;
            } else if (actual->valor < clave) {
                padreActual = actual;
                actual = actual->der;
            }
        }
        if (actual == NULL) {
            if (padreActual->valor < clave) {
                padreActual->der = nuevo;
            } else {
                padreActual->izq = nuevo;
            }
        }
        _cardinal++;
    }
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    if (pertenece(clave)) {
        if (_raiz->valor == clave) {
            Nodo* deadLast = _raiz;
            if (_raiz-> izq == NULL && _raiz->der == NULL){
                _raiz = NULL;
                delete deadLast;
            } else if (_raiz->izq != NULL && _raiz->der == NULL) {
                _raiz = _raiz->izq;
                delete deadLast;
            } else if (_raiz->izq == NULL && _raiz->der != NULL) {
                _raiz = _raiz->der;
                delete deadLast;
            } else {
                Nodo* Minimo = deadLast->der;
                while (Minimo->izq != NULL) {
                    Minimo = Minimo->izq;
                }
                Minimo->izq = deadLast->izq;

                _raiz = _raiz->der;
                delete deadLast;
            }
            _cardinal--;
        } else {

            Nodo *deadLast = _raiz;
            Nodo *padredeadLast = _raiz;

            while (clave != deadLast->valor) {
                if (clave < deadLast->valor) {
                    padredeadLast = deadLast;
                    deadLast = deadLast->izq;
                } else if (deadLast->valor < clave) {
                    padredeadLast = deadLast;
                    deadLast = deadLast->der;
                }
            }

            if (deadLast->izq == NULL && deadLast->der == NULL) {

                if (deadLast->valor < padredeadLast->valor) {
                    padredeadLast->izq = NULL; // A.1
                } else {
                    padredeadLast->der = NULL; // A.2
                }
                delete deadLast;
                _cardinal--;
            } else if (deadLast->izq != NULL && deadLast->der == NULL) {


                if (deadLast->valor < padredeadLast->valor) {
                    padredeadLast->izq = deadLast->izq; // B.1
                } else {
                    padredeadLast->der = deadLast->izq; // B.3
                }
                delete deadLast;
                _cardinal--;
            } else if (deadLast->izq == NULL && deadLast->der != NULL) {
                if (deadLast->valor < padredeadLast->valor) {
                    padredeadLast->izq = deadLast->der; // B.2
                } else {
                    padredeadLast->der = deadLast->der; // B.4
                }
                delete deadLast;
                _cardinal--;
            } else if (deadLast->izq != NULL && deadLast->der != NULL) {


                Nodo *Minimo = deadLast->der;
                while (Minimo->izq != NULL) {
                    Minimo = Minimo->izq;
                }
                Minimo->izq = deadLast->izq;
                // --- --- --- //

                if (deadLast->valor < padredeadLast->valor) {
                    padredeadLast->izq = deadLast->der; // C.1
                } else {
                    padredeadLast->der = deadLast->der; // C.2
                }
                delete deadLast;
                _cardinal--;
            }
        }
    }
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    Nodo* explorer = _raiz;
    Nodo* padreExplorer = _raiz;
    Nodo* Izquierdo = _raiz;


    while (clave != explorer->valor) {
        if (clave < explorer->valor) {
            padreExplorer = explorer;
            Izquierdo = explorer;
            explorer = explorer->izq;
        } else if (explorer->valor < clave) {
            padreExplorer = explorer;
            explorer = explorer->der;
        }
    }


    if (explorer->der != NULL) {
        explorer = explorer->der;
        while (explorer->izq != NULL) {
            explorer = explorer->izq;
        }

    } else if (explorer->valor < padreExplorer->valor) {
        explorer = padreExplorer;

    } else {
        explorer = Izquierdo;
    }
    return explorer->valor;
}

template <class T>
const T& Conjunto<T>::minimo() const {
    Nodo* actual = _raiz->izq;
    Nodo* min = _raiz;
    while (actual != NULL){
        min->valor = actual->valor;
        actual = actual->izq;
    }
    return min->valor;
}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo* actual = _raiz->der;
    Nodo* max = _raiz;
    while (actual != NULL){
        max->valor = actual->valor;
        actual = actual->der;
    }
    return max->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _cardinal;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}
