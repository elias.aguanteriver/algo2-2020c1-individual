#include <iostream>
#include "list"

using namespace std;

using uint = unsigned int;

typedef int Mes;
typedef int Dia;

const Mes ENERO = 1;
const Mes FEBRERO = 2;
const Mes MARZO = 3;
const Mes ABRIL = 4;
const Mes MAYO = 5;
const Mes JUNIO = 6;
const Mes JULIO = 7;
const Mes AGOSTO = 8;
const Mes SEPTIEMBRE = 9;
const Mes OCTUBRE = 10;
const Mes NOVIEMBRE = 11;
const Mes DICIEMBRE = 12;


int dias_en_mes(Mes mes){
    int result = 0;
    if(mes==2){
        result = 28;
    } else {
        if(mes==1 || mes==3 || mes==5 || mes==7 || mes==8 || mes==10 || mes==12){
            result = 31;
        } if(mes==4 || mes==6 || mes==9 || mes==11){
            result = 30;
        }
    }
    return result;
}
//



typedef int Meses;
typedef int Dias;


class Fecha {
public:
    Fecha(Mes mes, Dia dia);

    Mes mes() const;
    Dia dia() const;

    bool operator==(Fecha o) const;
    bool operator<(Fecha o) const;

    void incrementar_dia();


private:
    Mes _mes;
    Dia _dia;

    void sumar_dia(){
        _dia = 1 + _dia;
    };
};


Fecha::Fecha(Mes mes, Dia dia) :
         _mes(mes), _dia(dia){};


Mes Fecha::mes() const{
    return _mes;
}

Dia Fecha::dia() const {
    return _dia;
}

ostream& operator<<(ostream& os, Fecha f) {
    os << f.dia() << "/" << f.mes();
    return os;
}

bool Fecha::operator==(Fecha o) const {
    return (_mes==o.mes() && _dia==o.dia());
}

bool Fecha::operator<(Fecha o) const {
    return (_mes<o.mes()) ||  (_mes==o.mes() && _dia<o.dia());
}

void Fecha::incrementar_dia(){
    sumar_dia();
        if (_dia > 31 && (_mes == 1 || _mes == 3 || _mes == 5 || _mes == 7 || _mes == 8 || _mes == 10)) {
            _mes++;
            _dia = _dia - 31;
        }
        if (_dia > 30 && (_mes == 4 || _mes == 6 || _mes == 9 || _mes == 11)) {
            _mes++;
            _dia = _dia - 30;
        }
        if (_mes == 2 && _dia > 28) {
            _mes++;
            _dia = _dia - 28;
        }
        if (_dia > 31 && _mes == 12){
            _mes = 1;
            _dia = _dia - 31;
        }
}

typedef int Horas;
typedef int Minutos;


class Horario {
public:
    Horario(Horas hora, Minutos min);

    Horas hora() const;
    Minutos min() const;

    bool operator==(Horario o) const;
    bool operator<(Horario o) const;


private:
    Horas _hora;
    Minutos _min;

};

Horario::Horario(Horas hora, Minutos min) :
        _hora(hora), _min(min){};


Horas Horario::hora() const {
    return _hora;
}

Minutos Horario::min() const {
    return _min;
}

ostream& operator<<(ostream& os, Horario f) {
    os << f.hora() << ":" << f.min();
    return os;
}

bool Horario::operator==(Horario o) const {
    return (_hora==o.hora() && _min==o.min());
}


bool Horario::operator<(Horario o) const {
    return (_hora<o.hora() || (_hora==o.hora() && _min<o.min()));
}



typedef string Mensaje;

class Recordatorio {
public:
    Recordatorio(Fecha fecha, Horario horario, Mensaje mensaje);

    Fecha fecha() const;
    Horario horario() const;
    Mensaje mensaje() const;

    bool operator==(const Recordatorio& o) const;


private:
    Fecha _fecha;
    Horario _horario;
    Mensaje _mensaje;
};

Recordatorio::Recordatorio(Fecha fecha, Horario horario, Mensaje mensaje) :
        _fecha(fecha), _horario(horario), _mensaje(mensaje){};

bool Recordatorio::operator==(const Recordatorio& o) const {
    return (_fecha==o.fecha() && _horario==o.horario() && _mensaje==o.mensaje());
}

Fecha Recordatorio::fecha() const {
    return _fecha;
}

Horario Recordatorio::horario() const {
    return _horario;
}

Mensaje Recordatorio::mensaje() const {
    return _mensaje;
}

ostream& operator<<(ostream& os, Recordatorio f) {
    os << f.mensaje() << " @ " << f.fecha() << " " << f.horario();
    return os;
}




class Agenda {
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();
    list<Recordatorio> todos_los_recordatorios();


private:
    Fecha _hoy;
    list<Recordatorio> _record;


};

Agenda::Agenda(Fecha fecha_inicial) :
    _hoy(fecha_inicial), _record(){};

void Agenda::agregar_recordatorio(Recordatorio rec) {
    _record.push_back(rec);
}
void Agenda::incrementar_dia() {
     _hoy.incrementar_dia();
}

Fecha Agenda::hoy() {
    return _hoy;
}

list<Recordatorio> Agenda::todos_los_recordatorios() {
    return _record;
}

list<Recordatorio> Agenda::recordatorios_de_hoy() {
    list <Recordatorio> deHoy;
    list <Recordatorio> temp = _record;
    while (temp.size() >= 1) {
        if (temp.front().fecha() == _hoy) {
            deHoy.push_back(temp.front());
            temp.pop_front();
        } else {
            temp.pop_front();
        }
    }
    list <Recordatorio> porHora;
    while(deHoy.size()!=0){
        Recordatorio Ord = deHoy.front();
        Horario t = Ord.horario();
        for(int i=0; i < deHoy.size(); i++){
            if(deHoy.front().horario()<t){
                Ord = deHoy.front();
            }else{
                deHoy.push_back(deHoy.front());
                deHoy.pop_front();
            }
        }
        porHora.push_back(Ord);
        deHoy.remove(Ord);
    }
    return porHora;
}

ostream& operator<<(ostream& os, Agenda f) {
        list <Recordatorio> deHoy = f.recordatorios_de_hoy();
        os << f.hoy() << endl << "=====" << endl;
        while (deHoy.size()>=1) {
            os << deHoy.front() << endl;
            deHoy.pop_front();
        }
    return os;
}
