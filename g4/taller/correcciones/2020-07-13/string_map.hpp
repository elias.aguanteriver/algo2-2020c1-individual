template <typename T>
string_map<T>::string_map() : _claves() {
    raiz = nullptr;
    _size = 0;
    //linear_set<T> t;
    //_claves.operator=(t);
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
 string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    if (this == &d) {
        return *this;
    } else {
        reiniciar(this->raiz);
        raiz = copiar(d.raiz);
    }
    _claves = d.claves();
    return *this;
}

template <typename T>
typename string_map<T>::Nodo* string_map<T>::copiar(Nodo* d){
    if (d == nullptr) {
        return nullptr;
    } else {
        typename string_map<T>::Nodo* t = new Nodo;
        t->siguientes = d->siguientes;
        if (d->definicion) {
            t->definicion = new T(*d->definicion);
            _size++;
        } else {
            t->definicion = nullptr;
        }
        for (int i = 0; i <256; i++) {
            if(d->siguientes[i]) {
                t->siguientes[i] = copiar(d->siguientes[i]);
            }
        }
        return t;
    }

}


template <typename T>
string_map<T>::~string_map(){
    reiniciar(raiz);
    raiz = nullptr;
}
template <typename T>
void string_map<T>::reiniciar(Nodo* t) {
    if (t) {
        for (int i = 0;i<256;i++)
        {
            if(t->siguientes[i]) {
                reiniciar(t->siguientes[i]);
            }
        }
        if(t->definicion){
            delete t->definicion;
        }
        delete t;
        _size--;
    }
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    if (!raiz) {
        raiz = new Nodo;
        raiz->siguientes = vector<Nodo*>(256, nullptr);
        raiz->definicion = nullptr;
    }
    Nodo* actual = raiz;
    uint i = 0;
    while (i < clave.size()) {
        if(!actual->siguientes[int(clave[i])]) {
            Nodo* nuevo = new Nodo;
            nuevo->siguientes = vector<Nodo*>(256, nullptr);
            nuevo->definicion = nullptr;
            actual->siguientes[int(clave[i])] = nuevo;
            actual = actual->siguientes[int(clave[i])];
        } else {
            actual = actual->siguientes[int(clave[i])];
        }
        i++;
    }
    if (actual->definicion) {
        return *actual->definicion;
    } else {
        T* def = new T();
        actual->definicion = def;
        _size++;
        _claves.push_back(clave);
        return *actual->definicion;
    }
}
template<typename T>
void string_map<T>::insert(const pair<string, T> value_type) {
    if (!raiz) {
        raiz = new Nodo;
        raiz->siguientes = vector<Nodo*>(256, nullptr);
        raiz->definicion = nullptr;
    }
    Nodo* actual = raiz;
    uint i = 0;
    while (i < value_type.first.size()) {
        if(!actual->siguientes[int(value_type.first[i])]) {
            Nodo* nuevo = new Nodo;
            nuevo->siguientes = vector<Nodo*>(256, nullptr);
            nuevo->definicion = nullptr;
            actual->siguientes[int(value_type.first[i])] = nuevo;
            actual = actual->siguientes[int(value_type.first[i])];
        } else {
            actual = actual->siguientes[int(value_type.first[i])];
        }
        i++;
    } if(actual ->definicion){
        delete actual->definicion;
    }
        actual->definicion = new T(value_type.second);
        _size++;
        _claves.push_back(value_type.first);
}

/*
template <typename T>
T& string_map<T>::operator[](const string& clave) const{
    if (!raiz) {
        raiz = new Nodo;
        raiz->siguientes = vector<Nodo*>(256, nullptr);
        raiz->definicion = nullptr;
    }
    Nodo* actual = raiz;
    int i = 0;
    while (i < clave.size()) {
        if(!actual->siguientes[int(clave[i])]) {
            Nodo* nuevo = new Nodo;
            nuevo->siguientes = vector<Nodo*>(256, nullptr);
            nuevo->definicion = nullptr;
            actual->siguientes[int(clave[i])] = nuevo;
            actual = actual->siguientes[int(clave[i])];
        } else {
            actual = actual->siguientes[int(clave[i])];
        }
        i++;
    }
    if (actual->definicion) {
        return *actual->definicion;
    } else {
        T* def = new T();
        actual->definicion = def;
        _size++;
        _claves.insert(clave);
        return *actual->definicion;
    }
}
*/

template <typename T>
int string_map<T>::count(const string& clave) const{
    Nodo* actual = raiz;
    uint i = 0;
    if (!actual) {
        return 0;
    }
    while (i < clave.size() && actual->siguientes[int(clave[i])]) {
        actual = actual->siguientes[int(clave[i])];
        i++;
    }
    if (i == clave.size() && actual->definicion) {
        return 1;
    } else {
        return 0;
    }

}

template <typename T>
const T& string_map<T>::At(const string& clave) const {
    Nodo* actual = this->raiz;
    for (uint i = 0;i < clave.size();i++) {
        actual = actual->siguientes[int(clave[i])];
    }
    return *(actual->definicion);
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    Nodo* actual = this->raiz;
    for (uint i = 0;i < clave.size();i++) {
        actual = actual->siguientes[int(clave[i])];
    }
    return *(actual->definicion);
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    Nodo* ultimoNodo = this->raiz;
    unsigned int i = 0;
    while (i < clave.size()) {
        ultimoNodo = ultimoNodo->siguientes[int(clave[i])];
        i++;
    }
    unsigned int j = i;
    Nodo* actual = ultimoNodo;
    while (j > 0 && tieneNHijos(damePadre(clave,j),1)) {
        actual = damePadre(clave,j);
        j--;
    }
    if (actual == ultimoNodo || !tieneNHijos(ultimoNodo,0)) {
        delete(ultimoNodo->definicion);
        ultimoNodo->definicion = nullptr;
        _size--;
        for(int p = 0; p < this->_claves.size(); p++){
            if(this->_claves[p]==clave){
                _claves.erase(_claves.begin()+p);
            }
        }

    } else {
        delete(ultimoNodo->definicion);
        ultimoNodo->definicion = nullptr;
        while(i>j) {
            Nodo* aux = ultimoNodo;
            ultimoNodo = damePadre(clave,i);
            ultimoNodo->siguientes[int(clave[i])] = nullptr;
            delete aux;
            i--;
        }
        _size--;
        for(auto it = _claves.begin();it != _claves.end();it++){
            if((*it) == clave){_claves.erase(it);}
        }
    }
    if(i == 0) {
        raiz = nullptr;
    }

}

template <typename T>
typename string_map<T>::Nodo* string_map<T>::damePadre(const string& clave,int j){
    Nodo* actual = this->raiz;
    for (int i = 0;i < j;i++) {
        actual = actual->siguientes[int(clave[i])];
    }
    return actual;
}
template <typename T>
bool string_map<T>::tieneNHijos(Nodo* nodo,int n ){
    int suma = 0;
    for (int i = 0; i<256;i++) {
        if(nodo->siguientes[i]){
            suma++;
        }
    }
    return suma == n;
}
template <typename T>
int string_map<T>::size() const{
    return this->_size;
}

template <typename T>
bool string_map<T>::empty() const{
    return this->raiz == nullptr;
}

template <typename T>
const vector<string>& string_map<T>::claves() const {
    return this->_claves;
}